package deploy;

import com.prueba.modelo.Config;
import com.prueba.modelo.Persona;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext applicationContext=new AnnotationConfigApplicationContext(Config.class);

        Persona p= (Persona) applicationContext.getBean("persona");

        System.out.println(p.toString()+"");
        ((ConfigurableApplicationContext)applicationContext).close();

    }


}
