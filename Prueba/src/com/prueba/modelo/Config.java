package com.prueba.modelo;

import com.prueba.Impemetacion;
import com.prueba.Implementacio2;
import jdk.internal.dynalink.linker.LinkerServices;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
   public Persona persona(){
        return new Persona();
    }

    @Bean
    public Impemetacion impemetacion(){
        return  new Impemetacion();
    }


    @Bean
    public Implementacio2 implementacio2(){
        return new Implementacio2();
    }
}
