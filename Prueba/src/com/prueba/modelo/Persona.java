package com.prueba.modelo;

import com.prueba.IMensaje;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

public class Persona {
        @Value("1")
    private int id;
        @Value("el tuco")
    private String name;
         @Autowired
         @Qualifier("implementacio2")//diferenciar por nombre
        public IMensaje iMensaje;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", iMensaje=" + iMensaje.mensaje() +
                '}';
    }
}
